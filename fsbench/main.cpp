#include <unistd.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <chrono>

#define BUF_LEN 4096

using TimePoint = std::chrono::time_point<std::chrono::system_clock>;

TimePoint onStart = std::chrono::system_clock::now();

size_t nFiles = 0;
size_t totalRead = 0;

void read_file(const char* path)
{
     char chunk_buf[BUF_LEN];
     
     int fd = open(path, O_RDONLY);
     if(fd == -1) {
         perror("Unable to open file!");
         return;
     }
     
     ssize_t bytesRead = 0;
     
     do 
     {
         bytesRead = read(fd, chunk_buf, BUF_LEN);
         
         if (bytesRead > 0)
         {
             totalRead += bytesRead;
         }
     }
     while(bytesRead > 0);
     ++nFiles;
     
     close(fd);
}

void listdir(const char *name, int indent)
{
    DIR *dir;
    struct dirent *entry;

    if (!(dir = opendir(name)))
        return;

    while ((entry = readdir(dir)) != NULL) {        
        if (entry->d_type == DT_DIR) {
            char path[1024];
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
                continue;
            
            snprintf(path, sizeof(path), "%s/%s", name, entry->d_name);
            //printf("%*s[%s]\n", indent, "", entry->d_name);
            listdir(path, indent + 2);
        } else {
            std::string fullPath = name;
            fullPath += "/";
            fullPath += entry->d_name;
//          std::cout << fullPath << std::endl;
            
            read_file(fullPath.c_str());
            
            //printf("%*s- %s\n", indent, "", entry->d_name);
        }
    }
    
    TimePoint now = std::chrono::system_clock::now();   
    std::chrono::duration<double> difference = now - onStart;
    const double seconds = difference.count();
    double readRate = (double)totalRead / seconds;
    readRate /= 1024.;
    readRate /= 1024.;
    
    
    std::cout << "nFiles " << nFiles << " TotalRead " << totalRead  
              << " time " << seconds << " rate " << readRate << std::endl;
         
    closedir(dir);
}

int main(int argc, const char** argv) {
    if (argc != 2)
    {
        std::cout << "No path supplied" << std::endl;
        return 1;
    }
    
    listdir(argv[1], 0);
    return 0;
}