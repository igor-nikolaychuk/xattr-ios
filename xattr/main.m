#include <malloc/malloc.h>
#include <stdio.h>
#include <stdlib.h>

/*
 *  xattrfunctions.c
 *  little_lister
 *
 *  Created by Matthew Lye on 11/01/08.
 *  Copyright 2008 __MyCompanyName__. All rights reserved.
 *
 *  Using the xattr library calls.
 */

#include <sys/xattr.h>

//  define the option flags.
#define VERBOSE 0x0020
#define SYMBOLIC XATTR_NOFOLLOW
#define REPLACE XATTR_REPLACE
#define    CREATE XATTR_CREATE

int displayXattrKeys(const char *path, const char options);
int displayXattrValue(const char *path, const char *key, const char options);
int setXattrValue(const char *path, const char *key, const char *value, const char options);
int deleteXattr(const char *path, const char *key, const char options);


#include <stdio.h>
#include <stdlib.h>
#include <sys/xattr.h>
#include <sys/errno.h>
#include <string.h>


/*  Display Xattr Keys:
 
 This function displays the names of the extended attributes posessed by the file at 'path'.
 If the long/verbose option is set, it will call the displayXattrValue function,
 with the long/verbose option, for each extended attribute.
 
 */


int displayXattrKeys(const char *path, const char options) {
    
    int result=0;
    
    /*    BUFFER SIZES:
     The xattr functions return a negative value when they fail, assigning a diagnostic value to 'errno'.
     The xattr functions return a size value if called with a null buffer.
     */
    
    ssize_t bufferSize=listxattr(path, NULL, 0, options);                        // find out the buffer size.
    
    
    if (bufferSize < 0 ) { perror(NULL); return errno; }
    /*    LISTXATTR:
     The listxattr function fills the allocated text buffer with a list of attribute names,
     seperated by the null character ('\0').
     The function returns the number of attributes.
     */
    char *listBuffer=malloc(bufferSize);
    if (listBuffer == NULL) { perror(NULL); return errno; }                // return NOMEM on malloc fail.
    
    int listLength=listxattr(path, listBuffer, bufferSize, options);
    
    int n=0;                                                            // a character scanner counter.
    int nameStart=0;                                                    // the index position of each name.
    
    for (n=0; n < listLength; n++) {
        
        if ( listBuffer[n]=='\0' ) {                                    // at a null termination in the buffer...
            
            char *item=&listBuffer[nameStart];                            // return a pointer to the name...
            nameStart=n+1;                                                // and move to the next name.
            
            if ( VERBOSE & options)        result=result | displayXattrValue(path,item,options);
            else                        printf("%s\n",item);
            
        }
    }
    
    free(listBuffer);
    return result;
    
}


/*  Display Xattr Value:
 
 This function simply displays the value of the named extended attribute of the file at path.
 If the long/verbose option is set, it will return the name of the extended attribute and the value
 as "name=value", for use in shell scripts.
 
 getxattr does *not* null-terminate the strings it returns.  One must either use the buffer size information,
 or append a NULL.  I've done the latter.
 
 */


int displayXattrValue(const char *path, const char *key, const char options) {
    
    ssize_t bufferSize=getxattr(path, key, NULL, 0, 0,options);                    // check the buffer size.
    //printf("buffer size:  %i\n", bufferSize);
    if (bufferSize < 0 ) { perror(NULL); return errno; }                        // return NOMEM on malloc fail.
    char *valueBuffer=malloc(bufferSize + 1);                                // returns without nullterm, make room...
    if (valueBuffer == NULL) { perror(NULL); return errno; }
    
    valueBuffer[bufferSize]='\0';                                        // ...add nullterm.
    
    int valueLength=getxattr(path, key, valueBuffer, bufferSize, 0,options);
    if (valueLength < 0) { perror(NULL); return errno; }
    
    if ( VERBOSE & options )    printf("%s=",key);                        // prepend the attribute name.
    
    fprintf(stdout,"%s\n",valueBuffer);
    
    free(valueBuffer);
    return 0;
    
    
}

/*    Set Xattr Value:
 This function assigns values to new or existing extended attributes.  It responds to the '-R' and '-C' flags,
 which, respectively, prevent the creation of new attributes, or prevent the alteration of old ones.
 If the long/verbose option is set, displayXattrValue will be called with the long/verbose option.
 */

int setXattrValue(const char *path, const char *key, const char *value, const char options) {
    
    int valueSize=0;                                                        // the size of the value to be assigned.
    for (valueSize=0; value[valueSize]!='\0';valueSize++);                    // Does argv[] assure NULL term?
    
    if ( setxattr(path,key,value, valueSize,0, options) < 0) { perror(NULL); return errno; }
    if ( VERBOSE & options ) displayXattrValue(path, key, options);
    
    return 0;
    
}

/*    Delete Xattr:
 This function deletes the named extended attribute of the file at path.
 It will return an error if the attribute does not exist, or cannot be deleted.
 The deletion of the attribute is double-checked by a getxattr call that expects an error upon success.
 */

int deleteXattr(const char *path, const char *key, const char options){
    
    if ( removexattr(path, key, options) < 0 ) { perror(NULL); return errno; }
    int result=(getxattr(path, key, NULL, 0, 0, options)<0)?0:-1;            // check to see that it does not exist.
    
    if ( VERBOSE & options ) {
        
        if( result < 0 )    printf("Could not delete attribute '%s'.\n",key);
        else                printf("'%s' deleted.\n",key);
        
    }
    
    return result;
    
}


//command forms
#define LISTER 0
#define GETTER 1
#define SETTER 2
#define DELETE 3
#define HELP 4
#define VERSION 5

#define VERSION_NUMBER "0.32"

/*    GENERAL FLOW MODEL:
 
 (A)    Try to identify a command flag,
 (B)    Set all option bits and fail if an unknown flag is encountered,
 (C)    Iterate through remaining arguments, filling up variables in a predictable pattern, calling
 our xattr functions for every file listed.
 (D)    Terminate with a result of either 0 (PASS) or -1 (FAIL).
 
 */
void version() {
    printf("xattr (Leopardly) version %s is written by Matt Lye, released under the MIT liscense.\n", VERSION_NUMBER);
}
void usage()    {
    
    printf("usage:   xattr    [-sl] file [file ...]\n");
    printf("         xattr -p [-sl] attr_name file [file...]\n");
    printf("         xattr -w [-slRC] attr_name attr_value file [file...]\n");
    printf("         xattr -d [-sl] attr_name file [file...]\n");
    printf("         xattr      [-vh]\n\n");
    printf("The first form lists the name of all the extended attributes of the given file(s).\n");
    printf("The second form displays the value of the attribute 'attr_name' of the given file(s).\n");
    printf("The third form sets the value of the attribute 'attr_name' to 'attr_value' for the given file(s).\n");
    printf("The fourth form deletes the attribute 'attr_name' of the given file(s).\n\n");
    printf("options:  -s:  do not follow symbolic links.\n");
    printf("          -l:  long format (verbose) output in a key=value format.\n");
    printf("          -R:  replace existing values only.\n");
    printf("          -C:  create new keys only.\n");
    printf("          -v:  version information.\n");
    printf("          -h:  this help.\n");
}


int main (int argc, const char * argv[]) {
    
    
    /*    COMMAND FLAG:
     
     The absence of a command flag implies the 'list' command is being issued.
     The command and option formats are backwards-compatable with the 'xattr' python script shipped with Leopard.
     The command flag must be the first argument encountered.
     An unknown (non-command) flag will be assumed to be an option flag.
     
     */
    
    int command=LISTER;                                                //    The command flag default is to list attributes.
    
    
    if ( argc > 1 && argv[1][0]=='-') {                                //    Ensuring there is a flag argument.
        
        switch (argv[1][1]){
                
                
            case 'd':
                
                command = DELETE;
                break;
                
            case 'p':
                
                command = GETTER;
                break;
                
            case 'w':
                
                command = SETTER;
                break;
                
            case 'h':
                
                usage();                                            // ..the program terminates here.
                return -1;
                
            case 'v':
                
                version();                                            // ..the program terminates here.
                return -1;
                
            default:
                
                break;
                
        }
    }
    
    
    int n=1;                                                        // A counter for the 'argv' arguments.
    int m=1;                                                        // A counter for the characters in argv[n].
    char options='\0';                                                // A 'char' representing options as 1-bit flags.
    
    
    
    /*    OPTIONS FLAGS:
     All option flags currently MUST preceed all value arguments, for simplicity of design.
     
     Valid options are:
     
     l:    Display result in 'long' format.
     s:    Do NOT follow symbolic links.
     R:    ONLY re-write existing attributes.
     C:    ONLY create new attributes.
     
     */
    
    
    while (n<argc && argv[n][0]=='-') {                                // for as long as flags are encountered...
        
        m=1;
        
        while (argv[n][m]!='\0') {                                    // and until the end of this argv string...
            
            switch (argv[n][m]) {
                    
                    
                case 'l':
                    
                    options = options | VERBOSE;
                    break;
                    
                case 's':
                    
                    options = options | SYMBOLIC;
                    break;
                    
                case 'R':
                    
                    options = options | REPLACE;
                    break;
                    
                case 'C':
                    
                    options = options | CREATE;
                    break;
                    
                default:
                    
                    // An unknown command or option results in help display.
                    if (command == LISTER || n>1 ) usage();
                    break;
                    
            }                                                        // the end of the switch between option flags.
            
            m++;
            
        }                                                            // the character loop ('m' counter) closes.
        
        n++;
        
    }                                                                // the argv loop ('n' counter) closes.
    
    
    int keyPosition=0;
    int valPosition=0;
    int result=0;
    
    
    while ( n<argc ) {                                                // The value of 'n' is retained from previous loop.
        
        
        if ( command == LISTER ) result = result | displayXattrKeys(argv[n], options);
        else if (keyPosition == 0) keyPosition=n;
        else switch (command) {
            case GETTER:
                result=result | displayXattrValue(argv[n], argv[keyPosition], options);
                break;
            case SETTER:
                result=result | setXattrValue(argv[n], argv[keyPosition], argv[valPosition], options);
                break;
            case DELETE:
                result=result | deleteXattr(argv[n], argv[keyPosition], options);
                break;
            default:
                break;
        }
        
        
        n++;
        
    }                                                                    // the argv loop ('n' counter) closes.
    
    
    if (result<0 && command!=HELP) {                                    // if a problem has arisen...
        
        usage();
        return -1;                                                        // ..the program terminates here.
        
    }
    
    return 0;
    
}

